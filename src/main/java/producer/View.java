package producer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	CatalogView is a JFrame which contains the UI elements of the Client application.
 */
public class View extends JFrame {

    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JTextField textName;
    private JTextField textYear;
    private JTextField textPrice;
    private JButton addBtn;
    private JTextArea textArea;

    public View() {
        setTitle("New DVD wizard");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(300, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);


        JLabel lblName = new JLabel("Name");
        lblName.setBounds(135, 36, 46, 14);
        contentPane.add(lblName);

        textName = new JTextField();
        textName.setBounds(220, 33, 86, 20);
        contentPane.add(textName);
        textName.setColumns(10);

        JLabel lblPrice = new JLabel("Price");
        lblPrice.setBounds(135, 61, 60, 14);
        contentPane.add(lblPrice);

        textPrice = new JTextField();
        textPrice.setBounds(220, 61, 86, 20);
        contentPane.add(textPrice);
        textPrice.setColumns(10);

        JLabel lblYear = new JLabel("Year");
        lblYear.setBounds(135, 91, 60, 14);
        contentPane.add(lblYear);

        textYear = new JTextField();
        textYear.setBounds(220, 91, 86, 20);
        contentPane.add(textYear);
        textYear.setColumns(10);

        addBtn = new JButton("Add DVD");
        addBtn.setBounds(135, 120, 170, 23);
        contentPane.add(addBtn);

        textArea = new JTextArea();
        textArea.setBounds(25, 150, 400, 120);
        contentPane.add(textArea);

    }

    public void printStatus(String status) {
        textArea.setText(status);
    }

    public void addBtnActionListener(ActionListener e) {
        addBtn.addActionListener(e);
    }

    public String getName() {
        return textName.getText();
    }
    public String getYear() {
        return textYear.getText();
    }
    public String getPrice() {
        return textPrice.getText();
    }
    public void clear() {
        textName.setText("");
        textPrice.setText("");
        textYear.setText("");
        textArea.setText("");
    }
    public void clearTextArea(){
        textArea.setText("");
    }

}
