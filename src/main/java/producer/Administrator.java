package producer;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import common.DVD;
import org.apache.commons.lang3.SerializationUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.RemoteException;

public class Administrator {

    private static final String QUEUE1 = "Queue1";
    private static final String QUEUE2 = "Queue2";
    private static View view;

    public Administrator() {}

    static class NewDVDListener implements ActionListener {
        private Channel channel;
        public NewDVDListener(Channel c){
            channel = c;
        }
        public void actionPerformed(ActionEvent actionEvent) {
            String name = "";
            Integer year = 0;
            Double price = 0.0;
            try {
                view.clearTextArea();
                name = view.getName();
                year = Integer.parseInt(view.getYear());
                price = Double.parseDouble(view.getPrice());
                System.out.println(name);

                DVD d = new DVD(name, year, price);
                byte[] message = SerializationUtils.serialize(d);

                channel.basicPublish("", QUEUE1, null, message);
                channel.basicPublish("", QUEUE2, null, message);
                System.out.println(" [x] Sent '" + message + "'");

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e){
                view.clear();
                view.printStatus("Incorred input data!\n\n Try again");
            }
        }

    }

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        //channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

        channel.queueDeclare(QUEUE1, false, false, false, null);
        channel.queueDeclare(QUEUE2, false, false, false, null);

        view = new View();
        view.setVisible(true);
        view.addBtnActionListener(new NewDVDListener(channel));

        //channel.close();
        //connection.close();
    }


}
