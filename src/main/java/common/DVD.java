package common;

import java.io.Serializable;

/**
 * Created by constantin on 11/20/17.
 */
public class DVD implements Serializable {
    private static final long serialVersionUID = 7526472295622776147L;



    private String title;
    private int year;
    private double price;

    public DVD() {
    }

    public DVD(String title, int year, double price) {
        this.title = title;
        this.year = year;
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DVD dvd = (DVD) o;

        if (year != dvd.year) return false;
        if (Double.compare(dvd.price, price) != 0) return false;
        return title != null ? title.equals(dvd.title) : dvd.title == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = title != null ? title.hashCode() : 0;
        result = 31 * result + year;
        temp = Double.doubleToLongBits(price);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "comm.DVD" +
                "#" + title +
                "#" + year +
                "#" + price;
    }
}
