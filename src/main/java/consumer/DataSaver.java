package consumer;

import com.rabbitmq.client.*;
import common.DVD;
import org.apache.commons.lang3.SerializationUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

public class DataSaver {
    private static final String QUEUE = "Queue1";

    ConnectionFactory factory;
    Consumer consumer;
    Connection connection;
    Channel channel;
    String queueName;

    public DataSaver() throws Exception{
        factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare(QUEUE, false, false, false, null);
        init();
    }

    private void init() throws Exception{

        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                DVD message = (DVD) SerializationUtils.deserialize(body);
                System.out.println(" [x] Data Saver Received '" + message.getTitle() + "'");
                writeFile(message);
            }
        };

        channel.basicConsume(QUEUE, true, consumer);
    }

    private static void writeFile(DVD d) {
        try {
            PrintWriter writer = new PrintWriter("/home/constantin/Documents/sd/Movies/" + d.getTitle(), "UTF-8");
            writer.println(d.getTitle());
            writer.println(d.getYear());
            writer.println(d.getPrice());
            writer.close();

            System.out.println("File written");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}