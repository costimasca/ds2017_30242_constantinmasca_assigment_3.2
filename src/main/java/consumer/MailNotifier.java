package consumer;

import com.rabbitmq.client.*;
import common.DVD;
import org.apache.commons.lang3.SerializationUtils;

import java.io.IOException;

public class MailNotifier {
    private static final String QUEUE = "Queue2";

    ConnectionFactory factory;
    Consumer consumer;
    Connection connection;
    Channel channel;
    String queueName;
    MailService mailService;


    public MailNotifier() throws Exception{
        factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();
        channel = connection.createChannel();

        channel.queueDeclare(QUEUE, false, false, false, null);
        mailService = new MailService("rentboardgames1234@gmail.com", "12345678!");


        init();
    }

    public void init() throws Exception{


        consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                DVD d = (DVD)SerializationUtils.deserialize(body);
                System.out.println(" [x] Mail notifier Received '" + d.getTitle() + "'");
                    String mail = "New movie in store!\n" +
                            d.getTitle() + ", " +
                            d.getYear() + " for just " +
                            d.getPrice() + "$\nOrder it now!\n";
                    System.out.println("Sending mail:\n" + mail);

                    //mailService.sendMail("costimasca@gmail.com","New DVD added",mail);
            }
        };

        channel.basicConsume(QUEUE, true, consumer);
    }
}