package server;

import common.DVD;
import consumer.Consumer2;
import consumer.QueueConsumer;
import producer.Producer;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;

public class Main {
    public Main() throws Exception{

        QueueConsumer consumer = new QueueConsumer("queue");
        Consumer2 consumer2 = new Consumer2("queue");
        Thread consumer2Thread = new Thread(consumer2);
        Thread consumerThread = new Thread(consumer);

        consumer2Thread.start();

        //consumerThread.start();

        Producer producer = new Producer("queue");

            HashMap message = new HashMap();
            message.put("DVD", new DVD("Eyes wide shut",1998,24));
            producer.sendMessage(message);
    }

    /**
     * @param args
     * @throws SQLException
     * @throws IOException
     */
    public static void main(String[] args) throws Exception{
        new Main();
    }
}